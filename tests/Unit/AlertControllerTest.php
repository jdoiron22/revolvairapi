<?php

namespace Tests\Feature;

use App\Http\Controllers\AlertController;
use App\Http\Repositories\AlertRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Http\Requests\SensorReadingRequest;
use ReadingTypesTableSeeder;
use SensorReadingsTableSeeder;
use SensorsTableSeeder;
use StationsTableSeeder;
use Carbon\Carbon;
use App\Http\Repositories\SensorRepository;
use App\Http\Repositories\StationRepository;
use App\Http\Repositories\SensorReadingRepository;
use Mockery;
use App\Sensor;
use App\SensorReading;
use App\Http\Controllers\SensorReadingController;
use App\Station;
use App\Alert;



class AlertControllerTest extends TestCase
{
    use DatabaseTransactions;

    const STATION_ID = 1;
    const STATION_NAME = 'StationName';
    const STATION_CITY = 'StationCity';
    const STATION_LATITUDE = 10;
    const STATION_LONGITUDE = 90;
    const STATION_PUBLIC = 0;

    const FIND_WHERE_METHOD = 'findWhere';
    const FIND_BY_FIELD_METHOD = 'findByField';
    const GET_READING_TYPES_METHOD = 'getReadingTypes';
    const COLLECTION_WITH_RESULT = [1];

    private $station;
    private $stationRepositoryMock;
    private $sensorRepositoryMock;
    private $sensorReadingRepositoryMock;
    private $sensorReadingRequestMock;
    private $sensor;
    private $sensorReading;
    private $sensorReadingController;
    private $fakeReadingsPM2p5;
    private $alertController;
    private $alertRepositoryMock;

    public function setUp()
    {
        parent::setUp();

        $this->stationRepositoryMock = Mockery::Mock(StationRepository::class);
        $this->sensorRepositoryMock = Mockery::Mock(SensorRepository::class);
        $this->sensorReadingRepositoryMock = Mockery::Mock(SensorReadingRepository::class);
        $this->sensorReadingRequestMock = Mockery::Mock(SensorReadingRequest::class);
        $this->alertRepositoryMock = Mockery::Mock(AlertRepository::class);
        $this->sensor = new Sensor([
            "id" => SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID,
            "model_id" => 1,
            "station_id" => StationsTableSeeder::$PUBLIC_STATION_ID
        ]);
        $this->sensorReading = new SensorReading([
            'value' => SensorReadingsTableSeeder::$SENSOR_READING_2_VALUE,
            'sensor_id' => 1,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->alertController = new AlertController($this->stationRepositoryMock, $this->alertRepositoryMock);

        $this->sensorReadingController = new SensorReadingController
        (
            $this->stationRepositoryMock,
            $this->sensorRepositoryMock,
            $this->sensorReadingRepositoryMock,
            $this->alertController
        );

        $this->station = new Station([
            'id' => 1,
            'name' => self::STATION_NAME,
            'city' => self::STATION_CITY,
            'latitude' => self::STATION_LATITUDE,
            'longitude' => self::STATION_LONGITUDE,
            'is_private' => self::STATION_PUBLIC
        ]);

        // Fake readings
        $this->fakeReadingsPM2p5 = collect([]);
    }

    public function test__when_a_sensorReading_that_is_considered_dangereux_is_posted_then_alertRepository_should_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 500,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $alert = new Alert([
            'id' => 1,
            'station_id' => 2,
            'new_AQI' => 'Dangereux',
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldReceive('create')->andReturn($alert);
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }

    public function test__when_a_sensorReading_that_is_considered_tres_mauvais_is_posted_then_alertRepository_should_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 200,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $alert = new Alert([
            'id' => 1,
            'station_id' => 2,
            'new_AQI' => 'Très mauvais',
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldReceive('create')->andReturn($alert);
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }

    public function test__when_a_sensorReading_that_is_considered_mauvais_is_posted_then_alertRepository_should_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 130,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $alert = new Alert([
            'id' => 1,
            'station_id' => 2,
            'new_AQI' => 'Mauvais',
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldReceive('create')->andReturn($alert);
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }

    public function test__when_a_sensorReading_that_is_considered_mauvais_pour_les_groupes_sensibles_is_posted_then_alertRepository_should_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 100,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $alert = new Alert([
            'id' => 1,
            'station_id' => 2,
            'new_AQI' => 'Mauvais pour les groupes sensibles',
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldReceive('create')->andReturn($alert);
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }

    public function test__when_a_sensorReading_that_is_considered_modere_is_posted_then_alertRepository_should_not_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 50,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldNotReceive('create');
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }

    public function test__when_a_sensorReading_that_is_considered_bon_is_posted_then_alertRepository_should_not_create_an_alert()
    {
        $station_id = StationsTableSeeder::$PUBLIC_STATION_ID;
        $sensor_id = SensorsTableSeeder::$SENSOR_FROM_PUBLIC_STATION_ID;
        $request = new SensorReadingRequest([
            'id' => 1,
            'value' => 10,
            'latitude' => StationsTableSeeder::$STATION_1_LAT,
            'longitude' => StationsTableSeeder::$STATION_1_LON,
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        $this->stationRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(self::COLLECTION_WITH_RESULT);
        $this->stationRepositoryMock->shouldReceive(self::FIND_BY_FIELD_METHOD)->andReturn(collect([$this->station]));
        $this->stationRepositoryMock->shouldReceive('getReadings')->andReturn($this->fakeReadingsPM2p5);
        $this->sensorRepositoryMock->shouldReceive(self::FIND_WHERE_METHOD)->andReturn(collect($this->sensor));
        $this->sensorRepositoryMock->shouldReceive(self::GET_READING_TYPES_METHOD)->andReturn([ReadingTypesTableSeeder::$TYPE_PM2P5]);
        $this->sensorReadingRepositoryMock->shouldReceive('create')->andReturn($this->sensorReading);

        $this->alertRepositoryMock->shouldNotReceive('create');
        $this->sensorReadingController->store($station_id, $sensor_id, $request);
    }
}
