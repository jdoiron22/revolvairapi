<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Laravel\Passport\Passport;
use UsersTableSeeder;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogApiTest extends TestCase
{
    const ANY_STATUS_CODE = 404;
    const VALID_ERROR_MESSAGE = 'ERROR_MESSAGE';
    const ANY_VALID_URL = 'VALID_URL';
    const VALID_IP = '192.168.0.1';
    const ANY_USER_ID = 12;
    /**
     * A basic test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    const URL = '/api/logs';

    public function setUp()
    {
        parent::setUp();
    }

    public function test__Post_valid_log_error_should_succeed()
    {
        Passport::actingAs(\App\User::find(1));
        $response = $this->json(
            'POST', self::URL,
                    ['statusCode' => self::ANY_STATUS_CODE,
                    'errorMessage' => self::VALID_ERROR_MESSAGE,
                    'url' => self::ANY_VALID_URL,
                    'userId' => self::ANY_USER_ID]);
        $response->assertStatus(200);
    }

    public function test__Post_log_error_without_status_code_should_be_unsuccessful()
    {
        $response = $this->json(
            'POST', self::URL,
                    ['statusCode' => null,
                    'errorMessage' => self::VALID_ERROR_MESSAGE,
                    'url' => self::ANY_VALID_URL,
                    'userId' => null]);
        $response->assertStatus(422);
    }

    public function test__Post_log_error_containing_invalid_status_code_should_be_unsuccessful()
    {
        $response = $this->json(
            'POST', self::URL,
                    ['statusCode' => 'INVALID CODE',
                    'errorMessage' => self::VALID_ERROR_MESSAGE,
                    'url' => self::ANY_VALID_URL,
                    'userId' => null]);
        $response->assertStatus(422);
    }

    public function test__Post_log_error_containing_invalid_user_id_should_be_unsuccessful()
    {
        $response = $this->json(
            'POST', self::URL,
                    ['statusCode' => self::ANY_STATUS_CODE,
                    'errorMessage' => self::VALID_ERROR_MESSAGE,
                    'url' => self::ANY_VALID_URL,
                    'userId' => 'INVALID ID']);
        $response->assertStatus(422);
    }

    public function test__Post_log_error_without_error_message_should_be_unsuccessful()
    {
        $response = $this->json(
            'POST', self::URL,
                    ['statusCode' => self::ANY_STATUS_CODE,
                    'errorMessage' => '',
                    'url' => self::ANY_VALID_URL,
                    'userId' => 'INVALID ID']);
        $response->assertStatus(422);
    }

    public function test__Get_log_error_as_admin_should_succeed()
    {
        $response = $this->json('POST', '/api/login',
            ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $response = $this->json('GET', self::URL,
            [],
            ['HTTP_Authorization' => 'Bearer ' . $token]);
        $response->assertStatus(200);
    }

    public function test__Get_error_log_as_any_user_should_be_unsuccessful()
    {
        Passport::actingAs(\App\User::find(1));

        $response = $this->json('GET', self::URL,
            []);
        $response->assertStatus(403);
    }

    public function test__Get_error_log_as_guest_should_be_unsucccessful()
    {
        $response = $this->json('GET', self::URL, []);
        $response->assertStatus(401);
    }

    public function test__Get_user_id_should_return_id_of_current_user()
    {
        Passport::actingAs(\App\User::find(1));
            $response = $this->json('GET', 'api/userIdLogs',
                [], ['Content-Type'=> 'application/json']);
        $response->assertJsonFragment([1]);
    }
}
