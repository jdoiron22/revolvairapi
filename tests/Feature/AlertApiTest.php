<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AlertApiTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetAllAlerts()
    {
        $response = $this->get('/api/alerts');
        $response->assertStatus(200);
    }

    public function testGetAlertsFromSpecificStation()
    {
        $response = $this->get('/api/stations/1/alerts');
        $response->assertStatus(200);
    }

    public function testGetAllAlertsFromInvalidUrl()
    {
        $response = $this->get('/api/invalidUrl');
        $response->assertStatus(404);
    }

    public function testGetAlertsFromUnexistingStation()
    {
        $response = $this->get('/api/stations/10000000/alerts');
        $response->assertStatus(200);
        $response->assertJsonFragment([]);
    }
}
