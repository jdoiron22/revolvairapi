<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logs')->insert([
            [
                'statusCode' => 1408,
                'errorMessage' => 'VALIDE_ERROR_MESSAGE',
                'url' => 'https://testint.test/test/test',
		        'remoteIpAddress' => '192.2.1.3',
		        'HTTPXIpAddress' => '0.0.0.0',
		        'userId' => 2,
		        'created_at' => Carbon::now()->subHours(10)
            ]
        ]);
    }
}
