<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AlertsTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('alerts')->insert([
            [
                'station_id' => 1,
                'newAQI' => 'Mauvais pour les groupes sensibles',
                'created_at' => Carbon::yesterday()
            ]
        ]);

        DB::table('alerts')->insert([
            [
                'station_id' => 4,
                'newAQI' => 'Dangereux',
                'created_at' => Carbon::yesterday()
            ]
        ]);

        DB::table('alerts')->insert([
            [
                'station_id' => 5,
                'newAQI' => 'Mauvais',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
