<?php
/**
 * Created by IntelliJ IDEA.
 * User: nicolas
 * Date: 10/05/18
 * Time: 4:58 PM
 */

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class LogResource extends Resource
{
    public function toArray($request)
    {
        return [
            'statusCode' => $this->statusCode,
            'errorMessage' => $this->errorMessage,
            'url' => $this->url,
            'remoteIpAddress' => $this->remoteIpAddress,
            'HTTPXIpAddress' => $this->HTTPXIpAddress,
            'userId' => $this->userId,
            'created_at' => $this->created_at
        ];
    }
}