<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AlertResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'station_id' => $this->station->id,
            'station_name' => $this->station->name,
            'station_city' => $this->station->city,
            'new_AQI' => $this->newAQI,
            'created_at' => $this->created_at
        ];
    }
}
