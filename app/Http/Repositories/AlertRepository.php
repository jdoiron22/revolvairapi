<?php
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 24/04/18
 * Time: 9:40 AM
 */

namespace App\Http\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class AlertRepository extends BaseRepository {

    function model(){
        return "App\\Alert";
    }

    function getAlertByStation($station_id)
    {
        $this->orderBy('created_at', 'desc');
        return $this->findByField('station_id', $station_id);
    }
}
