<?php
/**
 * Created by IntelliJ IDEA.
 * User: nicolas
 * Date: 20/04/18
 * Time: 2:07 PM
 */

namespace App\Http\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;

class LogRepository extends BaseRepository
{
    function model()
    {
        return "App\\Log";
    }
}