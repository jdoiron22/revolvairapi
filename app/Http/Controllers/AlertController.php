<?php

namespace App\Http\Controllers;

use App\AQI\AQIRating;
use App\Http\Repositories\AlertRepository;
use App\Http\Repositories\StationRepository;
use App\Http\Resources\AlertResource;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    private $stationRepository;
    private $alertRepository;

    const BAD_AQI_FOR_SENSITIVE_GROUPS = "Mauvais pour les groupes sensibles";
    const BAD_AQI = "Mauvais";
    const REALLY_BAD_AQI = "Très mauvais";
    const DANGEROUS_AQI = "Dangereux";
    const QUEBEC_NORM_AQI_CLASS_PATH = "App\AQI\QuebecNormForAQI";

    public function __construct(StationRepository $stationRepository, AlertRepository $alertRepository)
    {
        $this->stationRepository = $stationRepository;
        $this->alertRepository = $alertRepository;
    }

    public function index()
    {
        return $this->alertRepository->with(['station'])->orderBy('created_at', 'desc')->paginate(50);
    }

    public function store(Request $request)
    {
        $alert = $this->alertRepository->create($request->all());
        return $alert;
    }

    public function show(int $station_id)
    {
        return AlertResource::collection($this->alertRepository->with(['station'])->getAlertByStation($station_id));
    }

    public function raiseAnAqiAlert($completeSensorReadingsInfos, $station_id)
    {
        $aqiValue = $this->getAqiValue($completeSensorReadingsInfos);

        $AQILabel = $this->getAQILabel($aqiValue);
        
        if ($this->verifyIfAlertShouldBeCreated($AQILabel)){
            $request = Request::create('alerts','POST', array('station_id' => $station_id, 'newAQI' => $AQILabel));
            $this->store($request);
        }
    }

    private function getAqiValue($completeSensorReadingsInfos)
    {
        $pollutantType = $completeSensorReadingsInfos["type"];

        $class = self::QUEBEC_NORM_AQI_CLASS_PATH . str_replace(".", "p", $pollutantType);
        $pollutantAQIClass = new $class;

        $pollutant = new $pollutantAQIClass();

        $pollutantValue = $completeSensorReadingsInfos["value"];

        $aqiValue = $pollutant->calculateAQI($pollutantValue);
        return $aqiValue;
    }

    private function getAQILabel(int $aqiValue)
    {
        $aqiRating = AQIRating::getRating($aqiValue);
        return $aqiRating["label"];
    }

    private function verifyIfAlertShouldBeCreated($AQILabel)
    {
        $response = false;

        switch ($AQILabel){
            case self::BAD_AQI_FOR_SENSITIVE_GROUPS:
                $response = true;
                break;
            case self::BAD_AQI:
                $response = true;
                break;
            case self::REALLY_BAD_AQI:
                $response = true;
                break;
            case self::DANGEROUS_AQI:
                $response = true;
                break;
        }

        return $response;
    }
}
