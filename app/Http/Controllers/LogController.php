<?php

namespace App\Http\Controllers;

use App\Log;
use App\Http\Repositories\LogRepository;
use App\Http\Requests\LogRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\LogResource;
use App\Http\Repositories\CommentRepository;
use App\Http\Controllers\AlertController;
use Illuminate\Http\Request;

class LogController extends Controller
{
    private $logRepository;

    public function __construct(LogRepository $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllLogs()
    {
        return $this->logRepository->orderBy('created_at', 'desc')->paginate(150);
    }

    public function getUserId()
    {
        $userId = 0;
        if(Auth::user())
        {
            $userId = Auth::user()->id;
        }
        return $userId;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogRequest $request)
    {
        $request['remoteIpAddress'] = $this->getRemoteIpAddress();
        $request['HTTPXIpAddress'] = $this->getForwardedIpAddress();
        $log = $this->logRepository->create($request->all());

        return $log;
    }

    private function getRemoteIpAddress()
    {
        $ip = '0.0.0.0';
        if(isset($_SERVER['REMOTE_ADDR']))
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private function getForwardedIpAddress()
    {
        $ip = '0.0.0.0';
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        return $ip;
    }
}
