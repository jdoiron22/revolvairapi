<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = ['station_id', 'newAQI'];

    public function station()
    {
        return $this->belongsTo('App\Station');
    }
}
