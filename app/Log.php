<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'statusCode',
        'errorMessage',
        'url',
        'remoteIpAddress',
        'HTTPXIpAddress',
        'userId',
        'date',
    ];
}
